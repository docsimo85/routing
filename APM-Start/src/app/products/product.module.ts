import {NgModule} from '@angular/core';

import {ProductListComponent} from './product-list.component';
import {ProductDetailComponent} from './product-detail.component';
import {ProductEditComponent} from './product-edit/product-edit.component';

import {SharedModule} from '../shared/shared.module';
import {ProductListResolverService} from './product-list-resolver.service';
import {ProductResolverService} from './product-resolver.service';
import {ProductEditGuard} from './product-edit/product-edit.guard';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: ProductListComponent,
        resolve: {resolvedData: ProductListResolverService}
      },
      {
        path: ':id',
        component: ProductDetailComponent,
        resolve: {product: ProductResolverService}
      },
      {
        path: ':id/edit',
        component: ProductEditComponent,
        resolve: {product: ProductResolverService},
        canDeactivate: [ProductEditGuard]
      },
      {
        path: 'new',
        component: ProductEditComponent,
      }
    ])
  ],
  declarations: [
    ProductListComponent,
    ProductDetailComponent,
    ProductEditComponent
  ]
})
export class ProductModule {
}
