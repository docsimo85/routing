import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ProductResolved} from './product';
import {ProductService} from './product.service';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<ProductResolved> {

  constructor(private productService: ProductService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<ProductResolved> {
    const id = route.paramMap.get('id');
    if (isNaN(+id)) {
      const message = `Product id was not a number: ${id}`;
      return of({product: null, error: message});
    }

    return this.productService.getProduct(+id)
      .pipe(
        map(product => ({product})),
        // tslint:disable-next-line:no-shadowed-variable
        catchError(error => {
          const message = `Retrieval error: ${error}`;
          console.log(message);
          return of({product: null, error: message});
        })
      );
  }
}
